<?php
/**
 * Created by PhpStorm.
 * Author: Egor Maznev  <egor260890@mail.ru>
 * Date: 07.02.2020
 * Time: 20:26
 */

namespace app\core\behaviors;

use app\core\search\entities\Product;
use yii\base\Behavior;
use yii\elasticsearch\ActiveRecord;

/**
 * Class ProductElasticsearchBehavior
 * @package app\core\behaviors
 */
class ProductElasticsearchBehavior extends Behavior
{

    /**
     * @return array
     */
    public function events(): array
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'onInsert',
            ActiveRecord::EVENT_AFTER_UPDATE => 'onUpdate',
            ActiveRecord::EVENT_AFTER_DELETE => 'onDelete',
        ];
    }

    public function onInsert(): void
    {
        $product = new Product();
        $product->id = $this->owner->id;
        $product->name = $this->owner->name;
        $product->save();
    }

    public function onUpdate(): void
    {
        $product = Product::findOne($this->owner->id);
        if ($product) {
            $product->id = $this->owner->id;
            $product->name = $this->owner->name;
            $product->save();
        }
    }

    public function onDelete(): void
    {
        if ($product = Product::findOne($this->owner->id)) {
            $product->delete();
        }
    }

}