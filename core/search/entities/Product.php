<?php
/**
 * Created by PhpStorm.
 * Author: Egor Maznev  <egor260890@mail.ru>
 * Date: 07.02.2020
 * Time: 11:53
 */

namespace app\core\search\entities;


use app\core\search\entities\queries\ProductQuery;
use yii\elasticsearch\ActiveQuery;
use yii\elasticsearch\ActiveRecord;
use yii\elasticsearch\Command;

/**
 * Class Product
 * @package app\core\search\entities
 */
class Product extends ActiveRecord
{

    /**
     * @return array
     */
    public static function primaryKey(): array
    {
        return ['id'];
    }

    /**
     * @return array
     */
    public function attributes(): array
    {
        return ['id', 'name'];
    }

    /**
     * @return string
     */
    public static function index(): string
    {
        return 'products';
    }

    public static function createIndex(): void
    {
        $command = new Command(['db' => \Yii::$app->elasticsearch]);
        if (!$command->indexExists(static::index())) {
            $command->createIndex(static::index(), null);
        }
    }

    public static function dropIndex(): void
    {
        $command = new Command(['db' => \Yii::$app->elasticsearch]);
        $command->deleteIndex(static::index());
    }

    /**
     * @return ProductQuery|ActiveQuery
     */
    public static function find(): ProductQuery
    {
        return new ProductQuery(get_called_class());
    }

}