<?php
/**
 * Created by PhpStorm.
 * Author: Egor Maznev  <egor260890@mail.ru>
 * Date: 07.02.2020
 * Time: 18:20
 */

namespace app\core\search\entities\queries;


use yii\elasticsearch\ActiveQuery;

/**
 * Class ProductQuery
 * @package app\core\search\entities\queries
 */
class ProductQuery extends ActiveQuery
{

    /**
     * @param string $attribute
     * @param string $query
     * @return ProductQuery
     */
    public function fuzzy(string $attribute, string $query): self
    {
        return $this->query([
            'fuzzy' => [
                $attribute => [
                    'value' => $query
                ]
            ]
        ]);
    }

    /**
     * @param string $attribute
     * @param string $query
     * @return ProductQuery
     */
    public function matchFuzzy(string $attribute, string $query): self
    {
        return $this->query([
            'match' => [
                $attribute => [
                    'query' => $query,
                    'fuzziness' => 'auto',
                ]
            ]
        ]);
    }
}