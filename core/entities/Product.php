<?php
/**
 * Created by PhpStorm.
 * Author: Egor Maznev  <egor260890@mail.ru>
 * Date: 07.02.2020
 * Time: 11:23
 */

namespace app\core\entities;


use app\core\behaviors\ProductElasticsearchBehavior;
use yii\db\ActiveRecord;

/**
 * Class Product
 * @package app\core\entities
 *
 * @property string $name
 */
class Product extends ActiveRecord
{

    /**
     * @param string $name
     * @return Product
     */
    public static function create(string $name): Product
    {
        $model = new Product();
        $model->name = $name;
        return $model;
    }

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            ProductElasticsearchBehavior::class
        ];
    }

    /**
     * @return string
     */
    public static function tableName(): string
    {
        return '{{%products}}';
    }

}