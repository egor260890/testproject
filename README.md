### Install with Docker

Go to docker folder

    cd docker
    
Install packages

    docker-compose run --rm php composer install    
    
Start the container

    docker-compose up -d
    
Configuration
-------------

Init configuration for docker

    docker-compose run --service-ports php php init --env=docker

Apply migrations  

    docker-compose run --service-ports php php yii migrate
    
Elasticsearch
-------------

Create index

    docker-compose run --service-ports php php yii product/create-index
    
Indexing
    
    docker-compose run --service-ports php php yii product/indexing
    
Add product

    docker-compose run --service-ports php php yii product/add "product name"
    
Search product
    
    docker-compose run --service-ports php php yii product/search "product name"
    
Others
-------------    

Drop index

    docker-compose run --service-ports php php yii product/drop-index
    
Delete products
    
    docker-compose run --service-ports php php yii product/delete "id"
    
Demo products
------------- 
    
    Смартфон Samsung Galaxy A51
    Смартфон Xiaomi Redmi Note 8T
    Смартфон Apple iPhone 11
    Смартфон HONOR 20 Pro
    Беззеркальный фотоаппарат Sony Alpha
    Фотоаппарат Panasonic Lumix
    Беззеркальный фотоаппарат Canon EOS
    