<?php

use yii\db\Migration;

/**
 * Class m200209_105312_insert_demo_products
 */
class m200209_105312_insert_demo_products extends Migration
{

    private $table = '{{%products}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert($this->table, ['name'], [
                ['Смартфон Samsung Galaxy A51'],
                ['Смартфон Xiaomi Redmi Note 8T'],
                ['Смартфон Apple iPhone 11'],
                ['Смартфон HONOR 20 Pro'],
                ['Беззеркальный фотоаппарат Sony Alpha'],
                ['Фотоаппарат Panasonic Lumix'],
                ['Беззеркальный фотоаппарат Canon EOS'],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete($this->table);
    }

}
