<?php

return [
    'class' => 'yii\elasticsearch\Connection',
    'nodes' => [
        ['http_address' => 'localhost:9200'],
    ],
];