<?php
/**
 * Created by PhpStorm.
 * Author: Egor Maznev  <egor260890@mail.ru>
 * Date: 07.02.2020
 * Time: 11:37
 */

namespace app\commands;


use app\core\entities\Product;
use app\core\search\entities\Product as ElasticProduct;
use yii\console\Controller;
use yii\helpers\ArrayHelper;

/**
 * Class ProductController
 * @package app\commands
 *
 * @property string $name
 */
class ProductController extends Controller
{

    /**
     * @param string $name
     * @throws \Exception
     */
    public function actionAdd(string $name): void
    {
        $product = Product::create($name);
        if ($product->save()) {
            echo "{$name} saved with ID: {$product->id}\n";
        } else {
            throw new \Exception('saving error');
        }
    }

    /**
     * products indexing
     */
    public function actionIndexing(): void
    {
        echo "Run indexing \n";
        foreach (Product::find()->each() as $record) {
            $product = new ElasticProduct();
            $product->id = $record->id;
            $product->name = $record->name;
            $product->save();
        }
        echo "Finish indexing \n";
    }

    /**
     * create elasticsearch index
     */
    public function actionCreateIndex(): void
    {
        ElasticProduct::createIndex();
    }

    /**
     * drop elasticsearch index
     */
    public function actionDropIndex(): void
    {
        ElasticProduct::dropIndex();
    }

    /**
     * @param string $query
     */
    public function actionSearch(string $query): void
    {
        $products = ElasticProduct::find()->matchFuzzy('name', $query)->all();
        if (!empty($products)) {
            $names = ArrayHelper::getColumn($products, 'name');
            echo "найдено записей: " . count($products) . "\n";
            echo implode("\n", $names);
        } else {
            echo "записи не найдены";
        }
        echo "\n";
    }

    /**
     * delete product
     * @param $id
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        if ($product = Product::findOne($id)) {
            echo \Yii::$app->formatter->asBoolean($product->delete());
        } else {
            echo "запись не найдена";
        }
        echo "\n";
    }

}