<?php
return [
    'docker' => [
        'path' => 'docker',
        'setWritable' => [
            'config',
        ],
    ],
];